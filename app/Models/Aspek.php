<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aspek extends Model
{
    protected $table = "aspeks";
    protected $primaryKey = "id";
    protected $fillable = [
        'id','nama','aspek','nilai','daftar_id'
    ];

    public function daftar()
    {
        return $this->belongsTo(Daftar::class);
    }

}
