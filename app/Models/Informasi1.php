<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Informasi1 extends Model
{
    use HasFactory;
    protected $table = 'informasi1s';
    protected $fillable = ['id','info','attachment'];
}
