<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NilaiAkhir extends Model
{
    protected $table = "nilai_akhirs";
    protected $primaryKey = "id";
    protected $fillable = [
        'id','nama','nilai_akhir'
    ];

    public function daftar()
    {
        return $this->belongsTo(Daftar::class);
    }

    public function provinsi()
    {
        return $this->belongsTo(Province::class, 'provinsi');
    }

    public function kabupaten()
    {
        return $this->belongsTo(Regency::class, 'kabupaten');
    }
    
    public function kecamatan()
    {
        return $this->belongsTo(District::class, 'kecamatan');
    }

    public function desa()
    {
        return $this->belongsTo(Village::class, 'desa');
    }

}
