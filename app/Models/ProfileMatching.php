<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfileMatching extends Model
{
    protected $table = "profile_matchings";
    protected $primaryKey = "id";
    protected $fillable = [
        'id','nama_id','kriteria_id','nilai'
    ];

    public function kriteriapenilaian(){
        return $this->belongsToMany(KriteriaPenilaian::class);
    }
    public function daftar(){
        return $this->belongsToMany(Daftar::class);
    }
}
