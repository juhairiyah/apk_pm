<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KriteriaPenilaian extends Model
{
    protected $table = "kriteria_penilaians";
    protected $primaryKey = "id";
    protected $fillable = [
        'id','aspek_id','kriteria','target','tipe'
    ];

    public function aspekpenilaian(){
        return $this->belongsTo('App\Models\AspekPenilaian');
    }

    public function profilematching(){
        return $this->belongsToMany(ProfileMatching::class, 'kriteria_id','id');
    }
}
