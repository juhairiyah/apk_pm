<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AspekPenilaian extends Model
{
    protected $table = "aspek_penilaians";
    protected $primaryKey = "id";
    protected $fillable = [
        'id','aspekpenilaian','presentase','corefactor','secondaryfactor'
    ];

    public function kriteriapenilaian(){
        return $this->hasMany(KriteriaPenilaian::class, 'aspek_id','id');
    }

   
}
