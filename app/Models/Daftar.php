<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Daftar extends Model
{
    protected $table = "daftars";
    protected $primaryKey = "id";
    protected $fillable = [
        'id','nama','jenis','nik','tempatlahir','tanggallahir','nohp','keterangan','provinsi','kabupaten','kecamatan','desa','foto_ktp','surat_kes','surat_non','email'
    ];

    public function provinsi()
    {
        return $this->belongsTo(Province::class, 'provinsi');
    }

    public function kabupaten()
    {
        return $this->belongsTo(Regency::class, 'kabupaten');
    }
    
    public function kecamatan()
    {
        return $this->belongsTo(District::class, 'kecamatan');
    }

    public function desa()
    {
        return $this->belongsTo(Village::class, 'desa');
    }

    public function profilematching(){
        return $this->belongsToMany(ProfileMatching::class, 'nama_id','id');
    }

    public function nilai()
    {
        return $this->hasOne(NilaiAkhir::class);
    }

    public function informasi1()
    {
        return $this->hasOne(Informasi1::class);
    }

}
