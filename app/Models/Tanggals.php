<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tanggals extends Model
{
    use HasFactory;
    protected $table = 'tanggals';
    protected $fillable = ['start_date', 'end_date'];
}
