<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
use App\Models\User;
use App\Models\Daftar;
use App\Models\AspekPenilaian;
use App\Models\KriteriaPenilaian;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register()
    {
        return view('register');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function actionlogin(Request $request)
    {
        $data = [
            'name' => $request->input('name'),
            'password' => $request->input('password'),
        ];

        if (Auth::Attempt($data)) {
            return redirect('home');
        }else{
            Session::flash('error', 'Email atau Password Salah');
            return redirect('/');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function home(Request $request) {
        $user = $request->User();

        if($user->role('Administrator')) {
            $data = Daftar::all();
            $count = $data->count();
            $jenis = Daftar::where('jenis', 'laki-laki')->count();
            $jenis1 = Daftar::where('jenis', 'perempuan')->count();
            $aspek = AspekPenilaian::all()->count();
            $kriteria = KriteriaPenilaian::all()->count();
            return view('admin/home', compact('data', 'count','jenis','jenis1','aspek','kriteria'));
        }
        elseif($user->role('User')) {
           return view('user/home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function actionregister(Request $request)
    // {
    //     $user = User::create([
    //         'name' => $request->name,
    //         'email' => $request->email,
    //         'password' => Hash::make($request->password),
    //         'role' => $request->role,
    //         'active' => 1
    //     ]);

    //     Session::flash('message', 'Register Berhasil. Akun Anda sudah Aktif silahkan Login menggunakan username dan password.');
    //     return redirect('register');
    // }

    // Memeriksa apakah email atau nama pengguna sudah terdaftar di dalam database
public function actionregister(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|unique:users|max:255',
            'email' => 'required|unique:users|email|max:255',
            'password' => 'required|min:8|max:255',
        ], [
            'name.unique' => 'Nama pengguna sudah terdaftar, silakan pilih nama pengguna yang berbeda.',
            'email.unique' => 'Email sudah terdaftar, silakan gunakan email yang berbeda.',
            'password.min' => 'password minimal 8 karakter.',
            'password.max' => 'password makasimal 225 karakter.',
        ]);
        // $passwords = User::pluck('password')->toArray();
        // if (in_array($request->password, $passwords)) {
        //     return 'Password yang dimasukkan sudah digunakan sebelumnya, silakan pilih password yang berbeda.';
        // }

        // Jika validasi berhasil, maka simpan data pengguna ke dalam database
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->role = $request->role;
        // $user->active = 1;
        $user->save();

        Session::flash('message', 'Register Berhasil. Akun Anda sudah Aktif silahkan Login menggunakan username dan password.');
        return redirect('register');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actionlogout()
    {
        Auth::logout();
        return redirect('/');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
