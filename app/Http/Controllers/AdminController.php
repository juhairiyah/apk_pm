<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Models\Daftar;
use App\Models\AspekPenilaian;
use App\Models\KriteriaPenilaian;
use App\Models\ProfileMatching;
use App\Models\Tanggal;
use App\Models\Aspek;
use App\Models\Informasi;
use App\Models\Informasi1;
use App\Models\NilaiAkhir;
use Session;
use PDF;

class AdminController extends Controller
{
    public function dashboard()
    {
        $data = Daftar::all();
        $count = $data->count();
        $jenis = Daftar::where('jenis', 'laki-laki')->count();
        $jenis1 = Daftar::where('jenis', 'perempuan')->count();
        $aspek = AspekPenilaian::all()->count();
        $kriteria = KriteriaPenilaian::all()->count();
        return view('admin/home', compact('data', 'count','jenis','jenis1','aspek','kriteria'));

    }

    public function datacalon()
    {
        $daftars = DB::table('daftars')
            ->join('provinces', 'daftars.provinsi', '=', 'provinces.id')
            ->join('regencies', 'daftars.kabupaten', '=', 'regencies.id')
            ->join('districts', 'daftars.kecamatan', '=', 'districts.id')
            ->join('villages', 'daftars.desa', '=', 'villages.id')
            ->select('daftars.*', 'provinces.name as provinsi_name', 'regencies.name as kabupaten_name', 'districts.name as kecamatan_name', 'villages.name as desa_name')
            ->get();

        return view('admin/datacalon',compact('daftars'));
    }

    public function lihatdatacalon($id)
    {
        $daftar = DB::table('daftars')
            ->join('provinces', 'daftars.provinsi', '=', 'provinces.id')
            ->join('regencies', 'daftars.kabupaten', '=', 'regencies.id')
            ->join('districts', 'daftars.kecamatan', '=', 'districts.id')
            ->join('villages', 'daftars.desa', '=', 'villages.id')
            ->select('daftars.*', 'provinces.name as provinsi_name', 'regencies.name as kabupaten_name', 'districts.name as kecamatan_name', 'villages.name as desa_name')
            ->where('daftars.id', $id)
            ->first();

        return response()->json([
            'status' => 'success',
            'daftar' => $daftar,
        ]);
    }

    public function hapusdatacalon ($id)
    {
        $daftar = Daftar::findorfail($id);
        $daftar->delete();
        return back();
    }


    public function perankingan()
    {
        $aspek = AspekPenilaian::all();
        return view('admin/perankingan', compact('aspek'));
    }

    public function tambahaspek()
    {
        return view('admin/aspekpenilaian/tambahaspekpenilaian');
    }

    public function tambahaspek1(Request $request)
    {
        AspekPenilaian::create([
            'aspekpenilaian' => $request-> aspekpenilaian,
            'presentase' => $request-> presentase,
            'corefactor' => $request-> corefactor,
            'secondaryfactor' => $request-> secondaryfactor,
        ]);
        Session::flash('message', 'Register Berhasil. Akun Anda sudah Aktif silahkan Login menggunakan username dan password.');
        return redirect('perankingan');
    }

    public function editaspek($id)
    {
        $aspek = AspekPenilaian::find($id);
        // dd($aspek);
        return view('admin/aspekpenilaian/editaspek',compact('aspek'));
    }

    public function simpaneditaspek(Request $request,$id)
    {
        $aspek = AspekPenilaian::find($id);
        $save = $aspek->update([
            'aspekpenilaian'=>$request->aspekpenilaian,
            'presentase'=>$request->presentase,
            'corefactor'=>$request->corefactor,
            'secondaryfactor'=>$request->secondaryfactor,
        ]);
        if ($save) {
            $aspek = AspekPenilaian::where('id')->first();
            return redirect()->route('perankingan');
        }
    }

    public function hapusaspek ($id)
    {
        $aspek = AspekPenilaian::findorfail($id);
        $aspek->delete();
        return back();
    }

    public function pengumuman()
    {
        $nilai = NilaiAkhir::join('provinces', 'nilai_akhirs.provinsi', '=', 'provinces.id')
        ->join('regencies', 'nilai_akhirs.kabupaten', '=', 'regencies.id')
        ->join('districts', 'nilai_akhirs.kecamatan', '=', 'districts.id')
        ->join('villages', 'nilai_akhirs.desa', '=', 'villages.id')
        ->select('nilai_akhirs.nama', 'provinces.name AS provinsi', 'regencies.name AS kabupaten', 'districts.name AS kecamatan', 'villages.name AS desa', 'nilai_akhirs.nilai_akhir','nilai_akhirs.keterangan','nilai_akhirs.id')
        ->orderByDesc('nilai_akhirs.nilai_akhir')
        ->get();
    
    //     $nilai = NilaiAkhir::with(['provinsi', 'kabupaten', 'kecamatan', 'desa'])
    // ->orderByDesc('nilai_akhir')
    // ->get();

        $daftar = Daftar::all();
        $info1 = Informasi1::all();
        $tanggal = Tanggal::all();
        $info = Informasi::all();

        // dd($nilai);
        return view('admin/pengumuman',compact('tanggal','info','nilai','daftar','info1'));
    }

    public function lolosAd($id)
    {
        $nilaiAkhir = NilaiAkhir::findOrFail($id);
        $nilaiAkhir->keterangan = 'Lolos';
        $nilaiAkhir->save();

        return redirect()->back();
    }

    public function printPDF()
    {
        $nilai = NilaiAkhir::join('provinces', 'nilai_akhirs.provinsi', '=', 'provinces.id')
            ->join('regencies', 'nilai_akhirs.kabupaten', '=', 'regencies.id')
            ->join('districts', 'nilai_akhirs.kecamatan', '=', 'districts.id')
            ->join('villages', 'nilai_akhirs.desa', '=', 'villages.id')
            ->select('nilai_akhirs.nama', 'provinces.name AS provinsi', 'regencies.name AS kabupaten', 'districts.name AS kecamatan', 'villages.name AS desa', 'nilai_akhirs.nilai_akhir', 'nilai_akhirs.keterangan')
            ->orderByDesc('nilai_akhirs.nilai_akhir')
            ->get();

            $pdf = PDF::loadView('admin.print_pdf', compact('nilai'))
                ->setPaper('A4', 'landscape'); // Set ukuran kertas A4 dan orientasinya landscape
        return $pdf->download('laporan_nilai.pdf');
    }


    public function setRegistrationPeriod(Request $request)
    {
        $request->validate([
            'start_date' => 'required|date',
            'end_date' => 'required|date|after_or_equal:start_date'
        ]);

        $user = Tanggal::firstOrNew(['id' => 1]); // hanya ada satu record untuk periode pendaftaran di tabel users
        $user->start_date = $request->input('start_date');
        $user->end_date = $request->input('end_date');
        $user->save();
        Session::flash('success', 'Periode Pendaftaran Sudah Selesai Diatur.');
        return redirect()->back();
    }

    public function informasi(Request $request)
    {
        $informasi = $request->input('informasi');

        if ($request->hasFile('attachment')) {
            $file = $request->file('attachment');
            $fileName = $file->getClientOriginalName();
            $path = $file->storeAs('attachments', $fileName, 'public');
            
            Informasi::create([
                'informasi' => $informasi,
                'attachment' => $path, // Simpan path file di dalam kolom 'attachment' pada tabel
            ]);
        } else {
            Informasi::create([
                'informasi' => $informasi,
            ]);
        }        

        Session::flash('message', 'Informasi berhasil ditambahkan');
        return redirect()->back();
    }

    public function informasi1(Request $request)
    {

        $info = $request->input('info');

        if ($request->hasFile('attachment')) {
            $file = $request->file('attachment');
            $fileName = $file->getClientOriginalName();
            $path = $file->storeAs('attachments', $fileName, 'public');
            
            Informasi1::create([
                'info' => $info,
                'attachment' => $path, // Simpan path file di dalam kolom 'attachment' pada tabel
            ]);
        } else {
            Informasi1::create([
                'info' => $info,
            ]);
        }        

        Session::flash('message1', 'Informasi berhasil ditambahkan');
        return redirect()->back();
    }


    public function kriteriapenilaian()
    {
        $kriteria = KriteriaPenilaian::all();
        return view('admin/kriteriapenilaian/kriteriapenilaian',compact('kriteria'));
    }

    public function tambahkriteria()
    {
        $aspek = AspekPenilaian::all();
        return view('admin/kriteriapenilaian/tambahkriteria', compact('aspek'));
    }

    public function tambahkriteria1(Request $request)
    {
        KriteriaPenilaian::create([
            'aspek_id' => $request-> aspek_id,
            'kriteria' => $request-> kriteria,
            'target' => $request-> target,
            'tipe' => $request-> tipe,
        ]);
        Session::flash('message', 'Register Berhasil. Akun Anda sudah Aktif silahkan Login menggunakan username dan password.');
        return redirect('kriteriapenilaian');
    }

    public function editkriteria($id)
    {
        $kriteria = KriteriaPenilaian::find($id);
        $aspek = AspekPenilaian::all();
        return view('admin/kriteriapenilaian/editkriteria', compact('kriteria','aspek'));
    }

    public function simpaneditkriteria(Request $request, $id)
    {
        $kriteria = KriteriaPenilaian::find($id);
        $save = $kriteria->update([
            'aspek_id' => $request-> aspek_id,
            'kriteria' => $request-> kriteria,
            'target' => $request-> target,
            'tipe' => $request-> tipe,
        ]);
        if ($save) {
            $kriteria = KriteriaPenilaian::where('id')->first();
            return redirect()->route('kriteriapenilaian');
        }
    }

    public function hapuskriteria ($id)
    {
        $kriteria = KriteriaPenilaian::findorfail($id);
        $kriteria->delete();
        return back();
    }

    public function hapusinfo ($id)
    {
        $info = Informasi::findorfail($id);
        $info->delete();
        return back();
    }

    public function hapusinfo1 ($id)
    {
        $info = Informasi1::findorfail($id);
        $info->delete();
        return back();
    }

    public function profilematching()
    {
        $aspek = AspekPenilaian::all();
        return view('admin/profilematching/profilematching',compact('aspek'));
    }

    public function jenisaspek(Request $request, $id)
    {
        $aspek = AspekPenilaian::all();
        $nama = Daftar::all();
        $jenis = AspekPenilaian::find($id);
        $pm = KriteriaPenilaian::where('aspek_id',$jenis->id)->get();
        // dd($pm);
        return view('admin/profilematching/prosespm', compact('nama','jenis','pm','aspek'));
    }

    public function simpandata(Request $request)
    {
        // looping untuk mendapatkan id kriteria dan id nama mahasiswa
        foreach ($request->except('_token') as $key => $value) {
            // memisahkan id kriteria dan id nama mahasiswa
            $keys = explode('_', $key);
            $kriteria_id = $keys[1];
            $nama_id = $keys[2];
            // menyimpan data ke database
            $data = [
                'kriteria_id' => $kriteria_id,
                'nama_id' => $nama_id,
                'nilai' => $value,
            ];
            ProfileMatching::create($data);
        }
        // redirect ke halaman index
        // dd($data);
        
        // Set pesan notifikasi ke session flash
        Session::flash('success', 'Data berhasil disimpan');
        return redirect()->back();

    }

    public function hasilperhitungan()
    {

        $nama = Daftar::all();
        $namas = Daftar::all();
        $aspeks = AspekPenilaian::all();
        $pm = ProfileMatching::latest()->get();
        $kriteria = KriteriaPenilaian::all();

        $tipeKriteria = KriteriaPenilaian::pluck('tipe', 'kriteria');

        $data = [];

        foreach ($aspeks as $aspek) {
            $kriterias = KriteriaPenilaian::where('aspek_id', $aspek->id)->get();

            foreach ($nama as $n) {
                $nilai = [];
                
                foreach ($kriterias as $k) {
                    $nilai[$k->kriteria] = [
                        'nilai' => $pm
                            ->where('nama_id', $n->id)
                            ->where('kriteria_id', $k->id)
                            ->first()
                            ->nilai,
                        'target' => $k->target,
                        'tipe' => $tipeKriteria[$k->kriteria],
                    ];
                }
                $data[$aspek->aspekpenilaian][$n->nama] = $nilai;
            }
        }       
        
        foreach ($data as $aspek => $nilai_nama){
                foreach($nilai_nama as $nama => $nilai){
                          $ncf = 0; // Inisialisasi nilai NCF
                          $ncfCount = 0; // Inisialisasi jumlah nilai "Core"
                          $nsf = 0; // Inisialisasi nilai NSF
                          $nsfCount = 0; // Inisialisasi jumlah nilai "Secondary"
                          $ni = 0; // Inisialisasi nilai N
                          
                          foreach($nilai as $kriteria=>$n){
                         
                          $selisih = $n['nilai'] - $n['target'];
                          if($selisih == 0) {
                            $selisih = 5.0;
                          } elseif($selisih == 1) {
                            $selisih = 4.5;
                          } elseif($selisih == -1) {
                            $selisih = 4.0;
                          } elseif($selisih == 2) {
                            $selisih = 3.5;
                          } elseif($selisih == -2) {
                            $selisih = 3.0;
                          } elseif($selisih == 3) {
                            $selisih = 2.5;
                          } elseif($selisih == -3) {
                            $selisih = 2.0;
                          } elseif($selisih == 4) {
                            $selisih = 1.5;
                          } elseif($selisih == -4) {
                            $selisih = 1.0;
                          }

                          if ($n['tipe'] == 'Core') {
                              $ncf += $selisih; // Menjumlahkan nilai $selisih jika tipe kriteria adalah "Core"
                              $ncfCount++; // Menghitung jumlah nilai "Core"
                          }elseif ($n['tipe'] == 'Secondary') {
                              $nsf += $selisih; // Menjumlahkan nilai $selisih jika tipe kriteria adalah "Secondary"
                              $nsfCount++; // Menghitung jumlah nilai "Secondary"
                          }                         

                        }
                        if ($ncfCount > 0) {
                            $ncf = $ncf / $ncfCount; // Menghitung rata-rata nilai "Core"
                          }
                          if ($nsfCount > 0) {
                            $nsf = $nsf / $nsfCount; // Menghitung rata-rata nilai "Secondary"
                          }
                          $ni = ($ncf * 60 / 100) + ($nsf * 40 / 100);
                        
                          $daftar = Daftar::where('nama', $nama)->first();

                          $aspekModel = Aspek::where('nama', $nama)
                            ->where('aspek', $aspek)
                            ->first();
                          
                            if ($aspekModel) {
                                // Update nilai pada model Aspek
                                $aspekModel->nilai = $ni;
                                $aspekModel->daftar()->associate($daftar);
                                $aspekModel->updated_at = now();
                                $aspekModel->save();
                            } else {
                                // Buat data baru jika tidak ditemukan
                                $aspekModel = new Aspek();
                                $aspekModel->nama = $nama;
                                $aspekModel->aspek = $aspek;
                                $aspekModel->nilai = $ni;
                                $aspekModel->daftar()->associate($daftar);
                                $aspekModel->created_at = now();
                                $aspekModel->updated_at = now();
                                $aspekModel->save();
                            }
                    }
                }

            
            
            $nilaiKriteria = ProfileMatching::join('kriteria_penilaians', 'profile_matchings.kriteria_id', '=', 'kriteria_penilaians.id')
            ->orderBy('kriteria_penilaians.tipe')
            ->orderBy('kriteria_penilaians.aspek_id')
            ->select('kriteria_penilaians.tipe','kriteria_penilaians.aspek_id', 'profile_matchings.nilai')
            ->get()
            ->groupBy('aspek_id');
            // dd($nilaiKriteria);
            $aspek2 = AspekPenilaian::pluck('aspekpenilaian')->toArray();
            $hasil = Aspek::whereIn('aspek', $aspek2)->get();
            $nilaiPerAspek = [];

            foreach ($hasil as $aspek) {
                $nilaiPerAspek[$aspek->nama][$aspek->aspek] = $aspek->nilai;
            }

            $hasil_awal = Aspek::all();
            $hasil_akhir = AspekPenilaian::all();
            $nilai_akhir = [];

            foreach ($hasil_awal as $item_awal) {
                
                $item_akhir = $hasil_akhir->where('aspekpenilaian', $item_awal->aspek)->first();
                if ($item_akhir) {
                    $presentase = $item_akhir->presentase;
                    $nilai = $item_awal->nilai * $presentase/100;

                    if (isset($nilai_akhir[$item_awal->nama])) {
                        // Jika nama sudah ada dalam array, tambahkan nilai ke total yang sudah ada
                        $nilai_akhir[$item_awal->nama] += $nilai;
                    } else {
                        // Jika nama belum ada dalam array, inisialisasi total dengan nilai awal
                        $nilai_akhir[$item_awal->nama] = $nilai;
                    }
                } else {
                    // Penanganan jika item dengan aspek yang sama tidak ditemukan
                    // Misalnya, tampilkan pesan error atau lakukan tindakan lain yang sesuai
                }
                
            }

            foreach ($nilai_akhir as $nama => $nilai) {
                $daftar = Daftar::where('nama', $nama)->first();
                if ($daftar) {
                    $nilaiAkhir = NilaiAkhir::where('nama', $nama)->first();
                    if ($nilaiAkhir) {
                        // Update existing record in NilaiAkhir model
                        $nilaiAkhir->nilai_akhir = $nilai;
                        $nilaiAkhir->daftar()->associate($daftar);
                        $nilaiAkhir->provinsi = $daftar->provinsi;
                        $nilaiAkhir->kabupaten = $daftar->kabupaten;
                        $nilaiAkhir->kecamatan = $daftar->kecamatan;
                        $nilaiAkhir->desa = $daftar->desa;
                        $nilaiAkhir->keterangan = "Tidak Lolos";
                        $nilaiAkhir->updated_at = now();
                        $nilaiAkhir->save();
                    } else {
                        // Create new record in NilaiAkhir model
                        $nilaiAkhir = new NilaiAkhir();
                        $nilaiAkhir->nama = $nama;
                        $nilaiAkhir->nilai_akhir = $nilai;
                        $nilaiAkhir->daftar()->associate($daftar);
                        $nilaiAkhir->provinsi = $daftar->provinsi;
                        $nilaiAkhir->kabupaten = $daftar->kabupaten;
                        $nilaiAkhir->kecamatan = $daftar->kecamatan;
                        $nilaiAkhir->desa = $daftar->desa;
                        $nilaiAkhir->keterangan = "Tidak Lolos";
                        $nilaiAkhir->created_at = now();
                        $nilaiAkhir->updated_at = now();
                        $nilaiAkhir->save();
                    }
                }
            }
            
            
            
            
            // dd($nilai_akhir);

        
        return view('admin/hasilperhitungan/hasilperhitungan', compact('data','namas','kriterias','kriteria','aspeks','pm','nilaiKriteria','tipeKriteria','hasil','nilaiPerAspek','aspek2','nilai_akhir'));
    }

}
