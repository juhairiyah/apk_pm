<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Province;
use App\Models\Regency;
use App\Models\District;
use App\Models\Village;
use App\Models\Daftar;
use App\Models\Tanggal;
use App\Models\Informasi;
use App\Models\Informasi1;
use App\Models\NilaiAkhir;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Validator;
use Session;

class UserController extends Controller
{
    public function home()
    {
        return view('user.home');
    }

    public function daftar()
    {
        $registration_period = Tanggal::select('start_date', 'end_date')->first();
        if ($registration_period && $registration_period->start_date <= date('Y-m-d') && $registration_period->end_date >= date('Y-m-d')) {
            $provinces = Province::all();
            return view('user.daftar',compact('provinces'));
        } else {
            // tampilkan pesan bahwa pendaftaran telah ditutup atau tampilkan halaman kosong
            return view('user.daftarkonfirmasi');
        }
    }

    public function pengumumanuser()
    {
        $info = Informasi::all();
        $info1 = Informasi1::all();
        
        $user = Auth::user();
        $daftar = Daftar::where('email', $user->id)->get();
        $nilai_akhir = NilaiAkhir::all();

        // dd($daftar);
        return view('user.pengumuman',compact('daftar','info1','info','nilai_akhir'));
    }

    public function lolos($id)
    {
        $nilaiAkhir = Daftar::findOrFail($id);
        $nilaiAkhir->keterangan = 'lolos';
        $nilaiAkhir->save();

        return redirect()->back();
    }

    public function tidak($id)
    {
        $nilaiAkhir = Daftar::findOrFail($id);
        $nilaiAkhir->keterangan = 'tidak';
        $nilaiAkhir->save();

        return redirect()->back();
    }

    public function getkabupaten(Request $request)
    {
        $id_provinsi = $request->id_provinsi;
        $kabupatens = Regency::where('province_id', $id_provinsi)->get();

        $option = "<option>Pilih Kabupaten...</option>";
        foreach($kabupatens as $kabupaten){
            $option.= "<option value='$kabupaten->id'>$kabupaten->name</option>";
        }        

        echo $option;
    }

    public function getkecamatan(Request $request)
    {
        $id_kabupaten = $request->id_kabupaten;
        $kecamatans = District::where('regency_id', $id_kabupaten)->get();

        $option = "<option>Pilih Kecamatan...</option>";
        foreach($kecamatans as $kecamatan){
            $option.= "<option value='$kecamatan->id'>$kecamatan->name</option>";
        }   
        
        echo $option;
    }

    public function getdesa(Request $request)
    {
        $id_kecamatan = $request->id_kecamatan;
        $desas = Village::where('district_id', $id_kecamatan)->get();

        $option = "<option>Pilih Desa...</option>";
        foreach($desas as $desa){
            $option.= "<option value='$desa->id'>$desa->name</option>";
        }       
        
        echo $option;
    }

    public function simpandaftar(Request $request)
    {
        $request->validate([
            'nama' => 'required|max:30',
            'jenis' => 'required',
            'nik' => 'required|numeric|min:16',
            'tempatlahir' => 'required|max:50',
            'tanggallahir' => 'required|date',
            'nohp' => 'required|numeric|min:11',
            'provinsi' => 'required',
            'kabupaten' => 'required',
            'kecamatan' => 'required',
            'desa' => 'required',
        ]);
        $request->validate([
            'foto_ktp' => 'nullable|mimes:jpg,jpeg,png,bmp,tiff,webp' // 1096 = 1mb,
        ]);
        
        $file = $request->file('foto_ktp');
        $imageName = 'ktp_' . uniqid() . '.' . $file->getClientOriginalExtension();
        $file->move(public_path('image'), $imageName);

        $request->validate([
            'surat_kes' => 'nullable|mimes:jpg,jpeg,png,bmp,tiff,webp' // 1096 = 1mb,
        ]);
        
        $file = $request->file('surat_kes');
        $imageName1 = 'surat_kes_' . uniqid() . '.' . $file->getClientOriginalExtension();
        $file->move(public_path('image'), $imageName1);

        $request->validate([
            'surat_non' => 'nullable|mimes:jpg,jpeg,png,bmp,tiff,webp' // 1096 = 1mb,
        ]);
        
        $file = $request->file('surat_non');
        $imageName2 = 'surat_non_' . uniqid() . '.' . $file->getClientOriginalExtension();
        $file->move(public_path('image'), $imageName2);

        Daftar::create([
            'nama' => $request-> nama,
            'jenis' => $request-> jenis,
            'nik' => $request-> nik,
            'tempatlahir' => $request-> tempatlahir,
            'tanggallahir' => $request-> tanggallahir,
            'nohp' => $request-> nohp,
            'provinsi' => $request-> provinsi,
            'kabupaten' => $request-> kabupaten,
            'kecamatan' => $request-> kecamatan,
            'desa' => $request-> desa,
            'foto_ktp'=>$imageName,
            'surat_kes'=>$imageName1,
            'surat_non'=>$imageName2,
            'keterangan'=>$request->keterangan,
            'email'=>$request->email,
        ]);
        return redirect('pengumumanuser')->with('message', 'Pendaftaran Berhasil. Terima kasih telah mendaftar untuk menjadi calon RAPALA. Tim seleksi kami sedang memeriksa semua aplikasi dan akan segera menghubungi Anda melalui informasi kontak yang Anda berikan. Harap diingat bahwa proses seleksi memerlukan waktu dan kami akan menghubungi Anda secepat mungkin setelah keputusan dibuat. Jangan ragu untuk menghubungi tim kami melalui informasi kontak yang tersedia di aplikasi ini jika Anda memiliki pertanyaan atau butuh bantuan lebih lanjut. Terima kasih atas kesediaan Anda untuk bergabung dengan tim RAPALA, dan semoga sukses dalam proses seleksi!');

    }
}
