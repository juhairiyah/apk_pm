<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
  </head>
  <body>
  
    <div class="card position-absolute top-50 start-50 translate-middle shadow-lg p-2 mb-5 rounded bg-white" style="width:300px;">
    <div class="card-body">
        <div class="text-center">
            <img src="{{asset('image/userrr.png')}}" class="rounded-circle" alt="..." style="width:70px;">
            <h3 class="mb-3" style="color:#0e466b">Welcome</h3>
        </div>
        <form method="post" action="{{route('actionlogin')}}">
            @csrf
            <div class="mb-3">
                <div class="input-group">
                    <div class="input-group-text " style="background-color:#0e466b"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-fill text-white" viewBox="0 0 16 16">
                    <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3Zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6Z"/>
                    </svg>
                    <i class="bi bi-person-fill"></i>
                    </div>
                    <input type="text" name="name" class="form-control" id="autoSizingInputGroup" placeholder="Username">
                </div>
            </div>
            <div class="mb-3">
                <div class="input-group">
                    <div class="input-group-text" style="background-color:#0e466b"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-key-fill text-white" viewBox="0 0 16 16">
                        <path d="M3.5 11.5a3.5 3.5 0 1 1 3.163-5H14L15.5 8 14 9.5l-1-1-1 1-1-1-1 1-1-1-1 1H6.663a3.5 3.5 0 0 1-3.163 2zM2.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                        </svg>
                    </div>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>
            </div>
            <div class="mb-1 text-center">
                <button type="submit" class="btn text-white" style="background-color:#0e466b">Login</button>
                <p class="mt-2" style="color:#0e466b">Belum punya akun? <a href="register" style="color:#0e466b">Register</a></p>
                <p text-muted style="color:#0e466b">&copy; 2022–2023</p>
            </div>
        </form>
    </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
  </body>
</html>