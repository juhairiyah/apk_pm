<?php

namespace Database\Seeders;

use App\Models\Aspek;

use Illuminate\Database\Seeder;

class HasilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Aspek::create([
            'nama'=>'ahmad syafii',
            'aspek'=>'Domisili dan Kesehatan',
            'nilai'=>'4',
        ]);
        Aspek::create([
            'nama'=>'husnul khatimah',
            'aspek'=>'Domisili dan Kesehatan',
            'nilai'=>'3.8',
        ]);
        Aspek::create([
            'nama'=>'muhammad ali',
            'aspek'=>'Domisili dan Kesehatan',
            'nilai'=>'4.2',
        ]);
        Aspek::create([
            'nama'=>'A SAMAT',
            'aspek'=>'Domisili dan Kesehatan',
            'nilai'=>'4.2',
        ]);
        Aspek::create([
            'nama'=>'Rudi Jamal',
            'aspek'=>'Domisili dan Kesehatan',
            'nilai'=>'4.2',
        ]);
        Aspek::create([
            'nama'=>'Juhairiyah',
            'aspek'=>'Domisili dan Kesehatan',
            'nilai'=>'4.2',
        ]);
        Aspek::create([
            'nama'=>'ahmad syafii',
            'aspek'=>'Sikap Kerja',
            'nilai'=>'4',
        ]);
        Aspek::create([
            'nama'=>'husnul khatimah',
            'aspek'=>'Sikap Kerja',
            'nilai'=>'3.8',
        ]);
        Aspek::create([
            'nama'=>'muhammad ali',
            'aspek'=>'Sikap Kerja',
            'nilai'=>'4.2',
        ]);
        Aspek::create([
            'nama'=>'A SAMAT',
            'aspek'=>'Sikap Kerja',
            'nilai'=>'4.2',
        ]);
        Aspek::create([
            'nama'=>'Rudi Jamal',
            'aspek'=>'Sikap Kerja',
            'nilai'=>'4.2',
        ]);
        Aspek::create([
            'nama'=>'Juhairiyah',
            'aspek'=>'Sikap Kerja',
            'nilai'=>'4.2',
        ]);
        Aspek::create([
            'nama'=>'ahmad syafii',
            'aspek'=>'Perilaku',
            'nilai'=>'4',
        ]);
        Aspek::create([
            'nama'=>'husnul khatimah',
            'aspek'=>'Perilaku',
            'nilai'=>'3.8',
        ]);
        Aspek::create([
            'nama'=>'muhammad ali',
            'aspek'=>'Perilaku',
            'nilai'=>'4.2',
        ]);
        Aspek::create([
            'nama'=>'A SAMAT',
            'aspek'=>'Perilaku',
            'nilai'=>'4.2',
        ]);
        Aspek::create([
            'nama'=>'Rudi Jamal',
            'aspek'=>'Perilaku',
            'nilai'=>'4.2',
        ]);
        Aspek::create([
            'nama'=>'Juhairiyah',
            'aspek'=>'Perilaku',
            'nilai'=>'4.2',
        ]);
    }
}
