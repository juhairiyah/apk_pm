<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDaftarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daftars', function (Blueprint $table) {
            $table->id();
            $table->char('provinsi', 2);
            $table->char('kabupaten', 4);
            $table->char('kecamatan', 7);
            $table->char('desa', 10);
            $table->string('nama');
            $table->string('jenis');
            $table->string('nik');
            $table->string('tempatlahir');
            $table->date('tanggallahir');
            $table->string('nohp');
            $table->string('foto_ktp');
            $table->string('surat_kes');
            $table->string('surat_non');
            $table->string('keterangan');
            $table->timestamps();
            $table->foreign('provinsi')->references('id')->on('provinces');
            $table->foreign('kabupaten')->references('id')->on('regencies');
            $table->foreign('kecamatan')->references('id')->on('districts');
            $table->foreign('desa')->references('id')->on('villages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daftars');
    }
}
