<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfileMatchingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_matchings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('nama_id');
            $table->unsignedBigInteger('kriteria_id');
            $table->string('nilai');
            $table->timestamps();
            $table->foreign('nama_id')->references('id')->on('daftars')->onupdate('cascade')->onDelete('cascade');
            $table->foreign('kriteria_id')->references('id')->on('kriteria_penilaians')->onupdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_matchings');
    }
}
