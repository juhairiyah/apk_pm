<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Aspek extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aspeks', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('aspek');
            $table->string('nilai');
            $table->timestamps();
        
            $table->unsignedBigInteger('daftar_id');
            $table->foreign('daftar_id')->references('id')->on('daftars')->onDelete('cascade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
