<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['web']], function () {
    Route::get('register', [LoginController::class, 'register'])->name('register');
    Route::post('actionlogin', [LoginController::class, 'actionlogin'])->name('actionlogin');
    Route::get('home', [LoginController::class, 'home'])->name('home')->middleware('auth');
    Route::post('actionregister', [LoginController::class, 'actionregister'])->name('actionregister');
    Route::get('actionlogout', [LoginController::class, 'actionlogout'])->name('actionlogout')->middleware('auth');
    
    Route::get('homeuser', [UserController::class, 'home'])->name('homeuser')->middleware('auth');
    Route::get('daftar', [UserController::class, 'daftar'])->name('daftar')->middleware('auth');
    Route::get('pengumumanuser', [UserController::class, 'pengumumanuser'])->name('pengumumanuser')->middleware('auth');
    
    Route::post('getkabupaten', [UserController::class, 'getkabupaten'])->name('getkabupaten')->middleware('auth');
    Route::post('getkecamatan', [UserController::class, 'getkecamatan'])->name('getkecamatan')->middleware('auth');
    Route::post('getdesa', [UserController::class, 'getdesa'])->name('getdesa')->middleware('auth');
    
    Route::post('simpandaftar', [UserController::class, 'simpandaftar'])->name('simpandaftar')->middleware('auth');
    
    Route::get('dashboard', [AdminController::class, 'dashboard'])->name('dashboard')->middleware('auth');
    Route::get('datacalon', [AdminController::class, 'datacalon'])->name('datacalon')->middleware('auth');
    Route::get('lihatdatacalon/{id}', [AdminController::class, 'lihatdatacalon'])->name('lihatdatacalon')->middleware('auth');
    Route::get('hapusdatacalon/{id}', [AdminController::class, 'hapusdatacalon'])->name('hapusdatacalon')->middleware('auth');
    Route::get('perankingan', [AdminController::class, 'perankingan'])->name('perankingan')->middleware('auth');
    Route::get('pengumuman', [AdminController::class, 'pengumuman'])->name('pengumuman')->middleware('auth');
    Route::post('registration.period.set', [AdminController::class, 'setRegistrationPeriod'])->name('registration.period.set')->middleware('auth');
    Route::post('informasi', [AdminController::class, 'informasi'])->name('informasi')->middleware('auth');
    Route::post('informasi1', [AdminController::class, 'informasi1'])->name('informasi1')->middleware('auth');
    Route::get('hapusinfo/{id}', [AdminController::class, 'hapusinfo'])->name('hapusinfo')->middleware('auth');
    Route::get('hapusinfo1/{id}', [AdminController::class, 'hapusinfo1'])->name('hapusinfo1')->middleware('auth');
    
    Route::get('tambahaspek', [AdminController::class, 'tambahaspek'])->name('tambahaspek')->middleware('auth');
    Route::post('tambahaspek1', [AdminController::class, 'tambahaspek1'])->name('tambahaspek1')->middleware('auth');
    Route::get('editaspek/{id}', [AdminController::class, 'editaspek'])->name('editaspek')->middleware('auth');
    Route::get('hapusaspek/{id}', [AdminController::class, 'hapusaspek'])->name('hapusaspek')->middleware('auth');
    Route::put('simpaneditaspek/{id}', [AdminController::class, 'simpaneditaspek'])->name('simpaneditaspek')->middleware('auth');
    
    Route::get('kriteriapenilaian', [AdminController::class, 'kriteriapenilaian'])->name('kriteriapenilaian')->middleware('auth');
    Route::get('tambahkriteria', [AdminController::class, 'tambahkriteria'])->name('tambahkriteria')->middleware('auth');
    Route::post('tambahkriteria1', [AdminController::class, 'tambahkriteria1'])->name('tambahkriteria1')->middleware('auth');
    Route::get('editkriteria/{id}', [AdminController::class, 'editkriteria'])->name('editkriteria')->middleware('auth');
    Route::put('simpaneditkriteria/{id}', [AdminController::class, 'simpaneditkriteria'])->name('simpaneditkriteria')->middleware('auth');
    Route::get('hapuskriteria/{id}', [AdminController::class, 'hapuskriteria'])->name('hapuskriteria')->middleware('auth');
    
    Route::get('profilematching', [AdminController::class, 'profilematching'])->name('profilematching')->middleware('auth');
    Route::get('jenisaspek/{id}', [AdminController::class, 'jenisaspek'])->name('jenisaspek')->middleware('auth');
    Route::post('simpandata', [AdminController::class, 'simpandata'])->name('simpandata')->middleware('auth');
    
    Route::get('hasilperhitungan', [AdminController::class, 'hasilperhitungan'])->name('hasilperhitungan')->middleware('auth');
    Route::get('lolosAd/{id}', [AdminController::class, 'lolosAd'])->name('lolosAd')->middleware('auth'); 
    Route::get('print-pdf', [AdminController::class, 'printPDF'])->name('print.pdf')->middleware('auth'); ;

    Route::get('lolos/{id}', [UserController::class, 'lolos'])->name('lolos')->middleware('auth'); 
    Route::get('tidak/{id}', [UserController::class, 'tidak'])->name('tidak')->middleware('auth'); 
    
});


